<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  function __construct()
	  {
	    parent::__construct(); 
	    $this->load->library('session');
	    $this->load->helper(array('form','url','html'));
	    $this->load->helper('security');
	    $this->load->database(); 
	    $this->load->model('Authmodel');	    
	  }
   

	public function userlogin()
	{
		$this->load->view('auth/userloginv');   
	}	 

	public function login_process()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date=date('Y-m-d');       
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->helper('security');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('Username', 'Username', 'trim|required|xss_clean|alpha_numeric');
		$this->form_validation->set_rules('Password', 'Password', 'trim|required|xss_clean');
		if ($this->form_validation->run() == FALSE)
		{	 
			$this->load->view('auth/userloginv');
		}
		else
		{  	
			$this->load->library('session');	
			$data = array(
				'Username' => $this->input->post('Username'),
				'Password' => $this->input->post('Password')
				);
			$this->load->database(); 
			$this->load->model('Authmodel');		
			$loginauth = $this->Authmodel->loginverfication($data);	
			$this->session->set_userdata('user_type',$loginauth[0]['user_type']);
			$this->session->set_userdata('user_id',$loginauth[0]['user_id']);
			$usertypeid=$this->session->userdata('user_id'); 
			if($loginauth)
        	{  

				if($this->session->userdata('user_type')=="7"){
				 $profiledetails= $this->Authmodel->get_vendor_profiledetails_superhead(); 
				}else if($this->session->userdata('user_type')=="4"){
					$profiledetails= $this->Authmodel->get_admin_profiledetails_superhead(); 
				} 
				 
				$this->session->set_userdata('name',$profiledetails['name']);
				$this->session->set_userdata('dp',$profiledetails['pic']);
				$this->session->set_userdata('address',$profiledetails['address']); 

        		if($this->session->userdata('user_type')=="7"){
				 redirect('Vendor/Vendor_home','refresh'); 
				} else if($this->session->userdata('user_type')=="4"){
	                redirect('Admin/Admin_home', 'refresh');
				}		

			}else{ 				 
				redirect('Auth/userlogin', 'refresh');
			}  
		}
	}

	public function logout()
	{
		$this->load->database();
        $this->load->library('session');
        $user_id = $this->session->userdata('user_id');
        if($user_id)
        {
	          $date=date('Y-m-d');
	          $time=date('H-i-s');
	          $datetime=$date.' '.$time;
	          $user_id = $this->session->userdata('user_id');
	          $this->load->model('Authmodel');
	          $data=array('logged_out'=>$datetime); 
        }
        $this->session->sess_destroy();
   		$this->session->unset_userdata('user_id'); 
   		redirect('Auth/userlogin', 'refresh');
	}

 
  

}