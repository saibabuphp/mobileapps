<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()    
  {
    parent::__construct(); 
    $this->load->library('session');
    $this->load->helper(array('form','url','html'));
    $this->load->helper('security');
    $this->load->database(); 
    $this->load->model('Mobilemodel');
    $this->load->model('Authmodel'); 

  }
	 
  public function getallposts(){ 
  	date_default_timezone_set('Asia/Calcutta');
  	$date = date('Y-m-d');
    $this->load->database(); 
    $this->load->model('Mobilemodel');	
    $data['results']= $this->Mobilemodel->getallposts($date);
    echo json_encode($data); 
  }
  public function getposts_ser_cat(){ 

  	if(isset($_GET ["service_type"]) && $_GET ["service_type"]!="" && isset($_GET ["category_type"]) && $_GET ["category_type"]!="" ){
  		date_default_timezone_set('Asia/Calcutta');
	  	$date = date('Y-m-d'); 
	    $data['results']= $this->Mobilemodel->getposts_ser_cat($date,$_GET ["service_type"],$_GET ["category_type"]);
	    echo json_encode($data); 
  	}
   
  }
  
  public function new_post_pic(){
		 
   	if(isset($_POST["service_type"]) && $_POST["service_type"]!="" && isset($_POST ["category_type"]) && $_POST ["category_type"]!="" && isset($_POST ["post_sub"]) && $_POST ["post_sub"]!="" && isset($_POST ["post_discription"]) && $_POST ["post_discription"]!="" && isset($_POST ["starting_date"]) && $_POST ["starting_date"]!="" && isset($_POST ["expired_date"]) && $_POST ["expired_date"]!="" ){
        
        $this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Calcutta');
		$date=date('Y-m-d');  
	    $currentmonth = date('m');
	    $currentdate  = date('d'); 
	    $currentyear  = date('y'); 
 		$key2='';
	     for($i=0;$i<2;$i++)
	     {
	        $key2.=rand(0,9);
	     }
	      $key='';
	     for($i=0;$i<1;$i++)
	     {
	         $key.=rand(0,9);
	     } 
		$post_id= $currentyear.$currentmonth.$currentdate.$_POST ["service_type"].$_POST ["category_type"].$key2.$key; 
		$pic11=$post_id."1";
		$pic22=$post_id."2"; 

			$icon = $this->input->post('pic1');  
        	 $path = $_FILES['pic1']['name'];
		     $ext = pathinfo($path, PATHINFO_EXTENSION); 
		     $pic1 = $pic11.'.'.$ext;
		     $output_dir1 = "assets/images/post/";
		     move_uploaded_file($_FILES["pic1"]["tmp_name"],$output_dir1.$pic1);

		     $icon1 = $this->input->post('pic2');  
        	 $path = $_FILES['pic2']['name'];
		     $ext = pathinfo($path, PATHINFO_EXTENSION); 
		     $pic2 = $pic22.'.'.$ext;
		     $output_dir1 = "assets/images/post/";
		     move_uploaded_file($_FILES["pic2"]["tmp_name"],$output_dir1.$pic2); 


            $data = array( 
            	'post_id'=>$post_id,
              'service_type'    => $_POST ["service_type"],
              'category_type'   => $_POST ["category_type"] ,
              'post_sub'		=> $_POST ["post_sub"],
              'post_discription'=> $_POST ["post_discription"],
              'starting_date'	=> $_POST ["starting_date"],
              'expired_date'	=> $_POST ["expired_date"], 
              'pic1'			=> $pic1,
              'pic2'			=> $pic2 
             );
		   $this->load->database();
		   $this->load->model('Mobilemodel');
		   $saved=$this->Mobilemodel->new_post($data); 
		    if($saved){
         	$response1 = array( 
             'Message'   => 'Saved sucessfully'        
             );
         	$data1['status']=TRUE;
		    $data1['results']=$response1;
		    echo json_encode($data1);
         }else{
         	$response1 = array( 
             'Message'   => 'something went wrong!'        
             );
         	$data1['status']=FALSE;
		    $data1['results']=$response1;
		    echo json_encode($data1);
         } 
		}else{
			$response1 = array( 
             'Message'   => 'Invalid details, Please Enter Valid post info..'        
             );
		$data1['status']=FALSE;
		$data1['results']=$response1;
		echo json_encode($data1);
		}  
	} 


  /*public function new_post(){
   if(isset($_GET ["service_type"]) && $_GET ["service_type"]!="" && isset($_GET ["category_type"]) && $_GET ["category_type"]!="" && isset($_GET ["post_sub"]) && $_GET ["post_sub"]!="" && isset($_GET ["post_discription"]) && $_GET ["post_discription"]!="" && isset($_GET ["starting_date"]) && $_GET ["starting_date"]!="" && isset($_GET ["expired_date"]) && $_GET ["expired_date"]!=""   ){
        
        $this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Calcutta');
		$date=date('Y-m-d');  
	    $currentmonth = date('m');
	    $currentdate  = date('d'); 
	    $currentyear  = date('y'); 
 		$key2='';
	     for($i=0;$i<2;$i++)
	     {
	        $key2.=rand(0,9);
	     }
	      $key='';
	     for($i=0;$i<1;$i++)
	     {
	         $key.=rand(0,9);
	     } 
		$post_id= $currentyear.$currentmonth.$currentdate.$_GET ["service_type"].$_GET ["category_type"].$key2.$key; 
            $data = array( 
            	'post_id'=>$post_id,
              'service_type'    => $_GET ["service_type"],
              'category_type'   => $_GET ["category_type"] ,
              'post_sub'		=> $_GET ["post_sub"],
              'post_discription'=> $_GET ["post_discription"],
              'starting_date'	=> $_GET ["starting_date"],
              'expired_date'	=> $_GET ["expired_date"] 
             );
		   $this->load->database();
		   $this->load->model('Mobilemodel');
		   $saved=$this->Mobilemodel->new_post($data); 
		    if($saved){
         	$response1 = array( 
             'Message'   => 'Saved sucessfully'        
             );
         	$data1['status']=TRUE;
		    $data1['results']=$response1;
		    echo json_encode($data1);
         }else{
         	$response1 = array( 
             'Message'   => 'something went wrong!'        
             );
         	$data1['status']=FALSE;
		    $data1['results']=$response1;
		    echo json_encode($data1);
         } 
		}else{
			$response1 = array( 
             'Message'   => 'Invalid details, Please Enter Valid post info..'        
             );
		$data1['status']=TRUE;
		$data1['results']=$response1;
		echo json_encode($data1);
		}  
	} */
	
	 
}
