<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()    
  {
    parent::__construct(); 
    $this->load->library('session');
    $this->load->helper(array('form','url','html'));
    $this->load->helper('security');
    $this->load->database(); 
    $this->load->model('Vendormodel');
    $this->load->model('Authmodel');
    date_default_timezone_set('Asia/Calcutta');
  	

  }
	public function Vendor_home()
	{	 
		 $data['post']=$this->Vendormodel->get_all_ven_post();
		$this->load->view('Vendor/Ven_dashboard',$data);
	}
	public function pending()
	{	 
		$date = date('Y-m-d'); 
		$data['ven_pending_post']=$this->Vendormodel->get_ven_pending_post($date); 
		$this->load->view('Vendor/Ven_ven_pending_post',$data);
	}
	public function live()
	{	 
		$date = date('Y-m-d'); 
		$data['ven_live_post']=$this->Vendormodel->get_ven_live_post($date); 
		$this->load->view('Vendor/Ven_ven_live_post',$data);
	}
	public function expired()
	{	 
		$date = date('Y-m-d'); 
		$data['ven_expired_post']=$this->Vendormodel->get_ven_expired_post($date); 
		$this->load->view('Vendor/Ven_ven_expired_post',$data);
	}
	public function V_create_post()
	{
       $data['service_type']=$this->Vendormodel->get_service_type();
       $data['category_type']=$this->Vendormodel->get_category_type(); 
		//var_dump($data);
        $this->load->view('Vendor/create_post',$data); 
	}
    







	 
}
