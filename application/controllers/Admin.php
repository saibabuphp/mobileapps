<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()    
  {
    parent::__construct(); 
    $this->load->library('session');
    $this->load->helper(array('form','url','html'));
    $this->load->helper('security');
    $this->load->database(); 
    $this->load->model('Adminmodel');
    $this->load->model('Authmodel'); 
  }
	public function Admin_home()
	{	 
		 
		$data['post']=$this->Adminmodel->get_all_post(); 
		$this->load->view('Admin/Admin_dashboard',$data);
	}
	/*public function new_post(){ 
        
        $this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Calcutta');
		$date=date('Y-m-d');  
	    $currentmonth = date('m');
	    $currentdate  = date('d'); 
	    $currentyear  = date('y'); 
 		$key2='';
	     for($i=0;$i<2;$i++)
	     {
	        $key2.=rand(0,9);
	     }
	      $key='';
	     for($i=0;$i<1;$i++)
	     {
	         $key.=rand(0,9);
	     } 
		$post_id= $currentyear.$currentmonth.$currentdate.$key2.$key;
		 

            $data = array( 
            	'post_id'=>$post_id,
              'service_type'    => "ser",
              'category_type'   => "category_type",
              'post_sub'		=> "post_sub",
              'post_discription'=> "post_discription",
              'starting_date'	=> "starting_date",
              'expired_date'	=> "expired_date" 
             );
		  
		   $saved=$this->Adminmodel->new_post($data); 
		    var_dump($saved); 
		 
	} 
    */
    public function pending()
	{	 $date = date('Y-m-d');  
		 $data['ven_pending_post']=$this->Adminmodel->get_adm_pending_post($date); 
		 $this->load->view('Admin/Adm_pending_post',$data);
	}
	public function live()
	{	 $date = date('Y-m-d'); 
		 $data['ven_live_post']=$this->Adminmodel->get_adm_live_post($date);
		 $this->load->view('Admin/Adm_live_post',$data);
	}
	public function expired()
	{	 $date = date('Y-m-d');  
		 $data['ven_expired_post']=$this->Adminmodel->get_adm_expired_post($date);
		 $this->load->view('Admin/Adm_expired_post',$data);
	}
	public function Adm_approved()
	{
		$refpostid=$this->input->post('post_id');
		$data = array(
					'a_post_display_flag' => 1,
					'post_status' => 1
					 );
		$this->Adminmodel->approve_post($refpostid,$data);
		redirect('Admin/live', 'refresh');
	}
	public function Adm_onhold()
	{
		$refpostid=$this->input->post('post_id');
		$data = array(
					'a_post_display_flag' => 0,
					'post_status' => 2
					 );
		$this->Adminmodel->post_to_onhold($refpostid,$data);
		redirect('Admin/pending', 'refresh');
	}
	public function Adm_edit()
	{
		exit("Stirng");
		$refpostid=$this->input->post('post_id');
		$this->Adminmodel->approve_req();
	}
	public function Adm_delete()
	{
		exit("Stirng");
		$refpostid=$this->input->post('post_id');
		$this->Adminmodel->approve_req();
	}






	 
}
