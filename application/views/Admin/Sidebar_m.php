<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md"> 
			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler --> 

			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							 
							<div class="mr-3">
								<a href="#"><img src="<?php echo base_url().'assets/images/user/'.$this->session->userdata('dp');?>" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold"><?php echo $this->session->userdata('name'); ?></div>
								<div class="font-size-xs opacity-50">
									<i class="icon-pin font-size-sm"></i> &nbsp;<?php echo $this->session->userdata('address'); ?>
								</div>
							</div> 
						</div> 
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion"> 
						<li class="nav-item nav-item-submenu">
							<a href="<?php echo base_url().'Admin/Admin_home';?>" class="nav-link active">
								<i class="icon-home4"></i>
								<span>
									Dashboard
								</span>
							</a>
						</li> 
						<li class="nav-item ">
							<a href="<?php echo base_url().'Admin/pending';?>" class="nav-link"><i class="icon-watch2"></i> <span>PENDING</span></a>
                        </li>
						<li class="nav-item  ">
							<a href="<?php echo base_url().'Admin/live';?>" class="nav-link"><i class="icon-copy"></i> <span>LIVE</span></a>
						</li>
                        <li class="nav-item">
							<a href="<?php echo base_url().'Admin/expired';?>" class="nav-link"><i class="icon-file-check"></i> <span>EXPIRED</span></a> 
						</li> 
						 
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-tree5"></i> <span>SALES</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Menu levels">
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> HOUSE</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-android"></i> PENDING</a></li>
										 
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> LIVE</a></li>
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> EXPIRE</a></li>
									</ul>
								</li> 
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> LAND</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-android"></i> PENDING</a></li>
										 
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> LIVE</a></li>
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> EXPIRE</a></li>
									</ul>
								</li> 
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> ELECTRONIC ITEMS</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-android"></i> PENDING</a></li>
										 
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> LIVE</a></li>
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> EXPIRE</a></li>
									</ul>
								</li> 
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-tree5"></i> <span>RENTAL</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Menu levels">
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> HOUSE</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-android"></i> PENDING</a></li>
										 
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> LIVE</a></li>
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> EXPIRE</a></li>
									</ul>
								</li> 
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> LAND</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-android"></i> PENDING</a></li>
										 
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> LIVE</a></li>
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> EXPIRE</a></li>
									</ul>
								</li> 
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-firefox"></i> ELECTRONIC ITEMS</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-android"></i> PENDING</a></li>
										 
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> LIVE</a></li>
										<li class="nav-item"><a href="#" class="nav-link"><i class="icon-windows"></i> EXPIRE</a></li>
									</ul>
								</li> 
							</ul>
						</li>

					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->