<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="<?php echo base_url();?>" class="d-inline-block">
				<img src="<?php echo base_url().'assets/images/logo1.png';?>" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<?php  $refval=$this->uri->segment(1,0);  
						if($refval!="Auth" || $refval =="" ){?>
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block"> 
						
					<i class="icon-paragraph-justify3"></i> 
					</a><?php }else { } ?> 
				</li> 
			</ul> 

			<span class="ml-md-3 mr-md-auto"> </span>
			 

			<ul class="navbar-nav">
				 
				<?php  
				if($this->session->userdata('user_type')=="7"){
				 $profiledetails= $this->Authmodel->get_vendor_profiledetails_superhead(); 
				}else if( $this->session->userdata('user_type')=="4"  ){
					$profiledetails= $this->Authmodel->get_admin_profiledetails_superhead();
				 
				}  

						if(!empty($profiledetails)) {  ?>
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						 <img src="<?php echo base_url().'assets/images/user/'.$this->session->userdata('dp');?>" class="rounded-circle mr-2" height="34" alt="">
						<span><?php  echo $this->session->userdata('name');   	?> 
 	 					</span>
					</a> 
					<div class="dropdown-menu dropdown-menu-right">
						 
						<a href="<?php echo base_url().'Auth/logout';?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div> 
				</li>
				<?php } else {  
					if($refval!="Auth" || $refval =="" ){
					?> 
				<li class="nav-item dropdown dropdown-user">
					<a style="color: #FFF;" href="<?php echo base_url().'Auth/userlogin';?>"  class="navbar-nav-link d-flex align-items-center ">
						<img src="<?php echo base_url().'assets/images/logo.png';?>" class="rounded-circle mr-2" height="34" alt="">
						<span>login</span>
					</a>  
				</li>
			<?php  }else {} }?>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
