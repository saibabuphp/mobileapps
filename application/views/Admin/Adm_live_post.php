 <?php $this->load->view('Admin/Headm_links'); ?>
<body>
	 <?php $this->load->view('Admin/Headm_navbar'); ?> 
	<!-- Page content -->
	<div class="page-content"> 
		 <?php $this->load->view('Admin/Sidebar_m'); ?>  
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline"> 
				</div> 
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline"> 	 
				</div> 
			</div>
			<!-- /page header -->  
			<!-- Content area -->
			<div class="content"> 
				  <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?> 
                	<div class="d-flex align-items-start flex-column flex-md-row"> 
					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1"> 
						<!-- Task grid -->
						<div class="row"> 
                        <?php //var_dump($post) ;exit();
                        if(!empty($ven_live_post)){ foreach($ven_live_post as $det){ ?> 
                 			<div class="col-xl-6">
								<div class="card border-left-3 border-left-success-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap"> 
											<div>
												 <?php 
													if (strlen($det['post_sub']) < 25) { $sub =  $det['post_sub'];
														} else { $sub = substr($det['post_sub'], 0, 25). ' &nbsp;&nbsp;<a href="#">...</a>';
														}
														if (strlen($det['post_discription']) < 25) { $subd =  $det['post_discription'];
														} else { $subd = substr($det['post_discription'], 0, 25). ' &nbsp;&nbsp; ';
														}  
														 ?>
												<h6><a href="<?php echo base_url().'Admin/assignsingleticket/'.$det['post_id'];?>">
													<?php echo $sub; ?></a></h6>
												<p class="mb-3"><?php echo $subd; ?></p> 
												<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
													<li> Category type: &nbsp;<?php    
													echo ucfirst($det['category_type']); ?>  </li>
												</ul>
                                                <span class="text-muted">Service type: <strong><?php echo ucfirst($det['service_type']); ?></strong></span>
											</div>
											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li><span class="text-muted"><?php  
												echo $updated = date('d F, Y', strtotime($det['updated']));	
												  ?></span></li>
												<li> Status: &nbsp;<?php    
													echo ucfirst($det['status']); ?>  </li>
												<li>
													<div class="btn-group">
									                    	<button type="button" class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false">Action</button>
									                    	<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-74px, -183px, 0px);min-width: 6.5rem !important;">
																<form method="post" action="<?php echo base_url().'Admin/Adm_onhold';?>">
									                    			<input type="hidden" name="post_id" value="<?php echo $det['post_id']; ?>">
									                    				<button type="submit" class="btn btn-success btn-sm dropdown-item"> Onhold</button>
									                    		</form>
																<form method="post" action="<?php echo base_url().'Admin/Adm_Edit';?>">
									                    			<input type="hidden" name="post_id" value="<?php echo $det['post_id']; ?>">
									                    			<button type="submit" class="btn btn-success btn-sm dropdown-item"> Edit Post</button>
									                    		</form>
																<div class="dropdown-divider"></div>
																<form method="post" action="<?php echo base_url().'Admin/Adm_delete';?>">
									                    			<input type="hidden" name="post_id" value="<?php echo $det['post_id']; ?>">
									                    			<button type="submit" class="btn btn-success btn-sm dropdown-item"> Delete</button>
									                    		</form>
															  </div>
													</div>
												</li>	
											</ul>
										</div>
									</div> 
									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>From: <span class="font-weight-semibold"><?php echo $starting_date = date('d F, Y', strtotime($det['starting_date']));	 ?></span></span> 
										<span>Expire on: <span class="font-weight-semibold"><?php echo $expired_date = date('d F, Y', strtotime($det['expired_date']));	 ?></span></span> 
									</div>
								</div>
							</div>
						<?php } }?> 
						</div>  
						<!-- Pagination --> 
						<!-- /pagination --> 
					</div>
					<!-- /left content --> 
				</div> 
				<!-- /main charts --> 
			</div>
			<!-- /content area -->
<?php $this->load->view('Admin/Footerm'); ?>
 

			