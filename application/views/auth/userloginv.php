 <?php $this->load->view('Admin/Headm_links'); ?>
<body>
	 <?php $this->load->view('Admin/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content"> 
		  
		<!-- Main content -->
		<div class="content-wrapper">  
			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center"> 
				<!-- Login card -->
				<form class="login-form form-validate" action="<?php echo base_url().'Auth/login_process';?>" method="post">
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								 
								<img width="100" src="<?php echo base_url().'assets/images/logo1.png';?>">
								<h5 class="mb-0">Login to your account</h5>
								<span class="d-block text-muted">Your credentials</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" name="Username" placeholder="Username" required>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" name="Password" placeholder="Password" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group d-flex align-items-center">
		 
								<a href="login_password_recover.html" class="ml-auto">Forgot password?</a>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div> 
						 
						</div>
					</div>
				</form>
				<!-- /login card --> 
			</div>
			<!-- /content area -->
<?php $this->load->view('Admin/Footerm'); ?>
 

			