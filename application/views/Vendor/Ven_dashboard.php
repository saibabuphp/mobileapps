 <?php $this->load->view('Admin/Headm_links'); ?>
<body>
	 <?php $this->load->view('Admin/Headm_navbar'); ?> 
	<!-- Page content -->
	<div class="page-content"> 
		 <?php $this->load->view('Vendor/Sidebar_m_v'); ?>  
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline"> 
				</div> 
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline"> 	 
				</div> 
			</div>
			<!-- /page header -->  
			<!-- Content area -->
			<div class="content"> 
				  <?php $this->load->view('Vendor/Ven_head_analytics_ticket_count'); ?> 
                	<div class="d-flex align-items-start flex-column flex-md-row"> 
					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1"> 
						<!-- Task grid -->
						<div class="row"> 
							<div class="col-xl-12 col-md-12 col-lg-12 col-sm-12">

						<!-- Marketing campaigns -->
						<div class="card">
							<div class="card-header header-elements-sm-inline">
								<div class="card-title "><a class="border-teal text-teal" href="<?php echo base_url().'Vendor/V_create_post/'; ?>"> NEW POST</a></div>  
								<?php
								if($this->session->flashdata('item')) {
								$message = $this->session->flashdata('item');?>
								<div class="<?php echo $message['class'] ?> "><?php echo $message['message']; ?> 
								</div>
								<?php }	?>
								<div class="header-elements  "> 
									<div class="list-icons ml-3">
				                		<div class="d-flex align-items-center justify-content-center mb-2">
											<a href="<?php echo base_url().'Vendor/V_create_post/'; ?>" class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
												<i class="icon-plus3"></i>
											</a>
											<div>
												<div class="font-weight-semibold">Rise post</div> 
											</div>
										</div>
				                	</div>
			                	</div>
							</div> 
						</div>
					</div>
                        <?php 
                        if(!empty($post)){ foreach($post as $det){ ?> 
                 			<div class="col-xl-6">
								<div class="card border-left-3 border-left-success-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap"> 
											<div>
												 <?php 
													if (strlen($det['post_sub']) < 25) { $sub =  $det['post_sub'];
														} else { $sub = substr($det['post_sub'], 0, 25). ' &nbsp;&nbsp;<a href="#">...</a>';
														}
														if (strlen($det['post_discription']) < 25) { $subd =  $det['post_discription'];
														} else { $subd = substr($det['post_discription'], 0, 25). ' &nbsp;&nbsp; ';
														}  
														 ?>
												<h6><a href="<?php echo base_url().'Admin/assignsingleticket/'.$det['post_id'];?>">
													<?php echo $sub; ?></a></h6>
												<p class="mb-3"><?php echo $subd; ?></p> 
                                                <span class="text-muted">Service type:<?php echo $det['service_type']; ?></span>
											</div>
											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li> Category type: &nbsp;<?php    
												echo $det['category_type']; ?>  </li>
												<li><span class="text-muted"><?php  
												echo $updated = date('d F, Y', strtotime($det['updated']));	 
												  ?></span></li> 
											</ul>
										</div>
									</div> 
									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>From: <span class="font-weight-semibold"><?php echo $starting_date = date('d F, Y', strtotime($det['starting_date']));	 ?></span></span> 
										<span>Expire on: <span class="font-weight-semibold"><?php echo $expired_date = date('d F, Y', strtotime($det['expired_date']));	 ?></span></span> 
									</div>
								</div>
							</div>
						<?php } }?> 
						</div>  
						<!-- Pagination --> 
						<!-- /pagination --> 
					</div>
					<!-- /left content --> 
				</div> 
				<!-- /main charts --> 
			</div>
			<!-- /content area -->
<?php $this->load->view('Admin/Footerm'); ?>
 

			