
<?php
class Adminmodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }

	function get_all_post() 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type,post.post_status')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function new_post($data){
        $rs=$this->db->insert('post',$data); 
        return($rs);
    }
     function get_adm_pending_post($date) 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type,post_status.status')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->join('post_status','post_status.id=post.post_status')
            ->where('post.post_status',2)
             ->where('post.a_post_display_flag',0) 
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function get_adm_live_post($date) 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type,post_status.status')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->join('post_status','post_status.id=post.post_status')
            ->where('post.post_status',1)
            ->where('post.a_post_display_flag',1)
            ->where('post.starting_date <=' ,$date)
            ->where('post.expired_date >=' ,$date)
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function get_adm_expired_post($date) 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->where('post.post_status',1)
            ->where('post.a_post_display_flag',1)
            ->where('post.expired_date  <',$date)
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function approve_post($post_id,$data)
    {
        $res=$this->db->where('post_id',$post_id)
             ->update('post',$data);
            return($res);
    }
    function post_to_onhold($post_id,$data)
    {
        $res=$this->db->where('post_id',$post_id)
             ->update('post',$data);
            return($res);
    } 
     
}