
<?php
class Mobilemodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }

	function getallposts($date)
    {	
    	$res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->where('post.post_status',1)
            ->where('post.post_display_flag',1)
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result();
        return($res);      
    }
     function new_post($data){
        $rs=$this->db->insert('post',$data);
       return $rs;
    }
    function getposts_ser_cat($date,$service_type,$category_type)
    {	
    	$res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->where('post.post_status',1)
            ->where('post.service_type',$service_type)
            ->where('post.category_type',$category_type)
            ->where('post.a_post_display_flag',1)
            ->where('post.starting_date <=',$date)
            ->where('post.expired_date  >=',$date)
            ->group_by('post.post_id')
            ->order_by("post.id","desc")->get()->result();
        return($res);      
    }
     
}