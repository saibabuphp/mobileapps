
<?php
class Vendormodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }
    function get_all_ven_post() 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            
            ->where('post.user_id',$this->session->userdata('user_id'))
            
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function get_ven_pending_post($date) 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->where('post.post_status',0)
            ->where('post.user_id',$this->session->userdata('user_id'))
            ->where('post.a_post_display_flag',0) 
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function get_ven_live_post($date) 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->where('post.post_status',1)
            ->where('post.user_id',$this->session->userdata('user_id'))
            
           ->where('post.starting_date <=' ,$date)
            ->where('post.expired_date >=' ,$date)
            ->where('post.a_post_display_flag',1)
            ->where('post.post_status',1)
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function get_ven_expired_post($date) 
    {
        $res=$this->db->select('post.post_sub,post.updated,post.post_id,post.post_discription,post.starting_date,post.expired_date,service_type.service_type ,category_list.category_type')     
            ->from('post')
            ->join('category_list','category_list.id=post.category_type')
            ->join('service_type','service_type.id=post.service_type')
            ->where('post.post_status',1)
            ->where('post.user_id',$this->session->userdata('user_id'))
            ->where('post.a_post_display_flag',1) 
            ->where('post.expired_date  <',$date)
            ->group_by('post.post_id')
            ->order_by("post.id","desc") 
            ->get()->result_array();
        return($res);  
    }
    function get_service_type(){
        $res=$this->db->get('service_type');
        return($res);
    }
    function get_category_type(){
        $res=$this->db->get('category_list');
        return($res);
    }
     
}